#ifndef TYPES_H_
#define TYPES_H_

#include <stdint.h>
#define N 2                            //size of neighbors matrix


typedef struct list_node *Buffer;

typedef unsigned long long ptr;

struct list_node{

    uint32_t neighbor[N];               //the ids of the neighbor nodes
    uint32_t edgeProperty[N];           //property of each node
    ptr nextListNode;    //offset of next node

};

unsigned long long M;             //size of buffer (max number of listnodes)

Buffer buf;

ptr position;                     //current position (offset) of first empty position of buffer

#endif
