#include "buffer.h"

int main(){

    int i, j;
    Buffer* buff;
    Buffer test;

    M = 3;

    printf("\nStart of program for testing buffer...\n\n");

    //phase1 creation and test of buffer
    buff = createBuffer();
    printf("A new buffer was created with space for %llu list_nodes and %d elements per node\n", M, N);

    test = *buff;

    for(i=1; i<=M; i++){
        for(j=0; j<N; j++){
            printf("Give i:");
            scanf("%u", &(test->neighbor[j]) );
        }
        test++;
    }

    printf("\nPrinting all elements...\n");
    test = *buff;
    for(i=1; i<=M; i++){
        for(j=0; j<N; j++)
            printf("list No %d, element No %d: %u\n", i, j, test->neighbor[j]);
        test++;
    }


    //phase2 reallocation and test of buffer //
    M *= 2;
    if( (*buff = (struct list_node *) realloc(*buff, M*sizeof(struct list_node))) == NULL ){
        printf("failure: Can not reallocate memory for a Buffer.\n");
        return -1;
    }

    printf("\nBuffer now has space for %llu list_nodes and %d elements per node\n", M, N);

    test = *buff;

    for(i=1+M/2; i<=M; i++){
        for(j=0; j<N; j++){
            printf("Give i:");
            scanf("%u", &((test+M/2)->neighbor[j]) );
        }
        test++;
    }

    printf("\nPrinting all elements...\n");
    test = *buff;
    for(i=1; i<=M; i++){
        for(j=0; j<N; j++)
            printf("list No %d, element No %d: %u\n", i, j, test->neighbor[j]);
        test++;
    }


    //end of program
    if(destroyBuffer(buff)){
        printf("failure: Can not free memory of a Buffer.\n");
        return -1;
    }
    printf("\nThe program ended normaly!\n\n");

    return 0;
}
