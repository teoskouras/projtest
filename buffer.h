#ifndef BUFFER_H_
#define BUFFER_H_

#include <stdio.h>
#include <stdlib.h>
#include "types.h"


Buffer *createBuffer();

ptr allocNewNode(Buffer*);

struct list_node* getListNode(ptr);

int destroyBuffer(Buffer*);


#endif
