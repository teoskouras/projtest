#include "buffer.h"

//*** Allocate space for a new buffer of a standard size of M list_nodes ***//
Buffer* createBuffer(){

    if( (buf = (struct list_node *) malloc(M*sizeof(struct list_node))) == NULL )
        printf("failure: Can not allocate memory for a new Buffer.\n");

    position = 0;               //initialize position of first empty space

    return &buf;
}

//*** Find next empty space in the buffer,to save a new list_node ***//
ptr allocNewNode(Buffer* bf){

    return position++;          //return current position of first empty space
}

//*** Find the address of a list_node giving its offset ***//
struct list_node* getListNode(ptr offset){



}

//*** Free the allocated space of a buffer ***//
int destroyBuffer(Buffer* b){

    free(*b);

    return 0;               //return the corresponding number of message "OK_SUCCESS"
}

